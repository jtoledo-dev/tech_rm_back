
const PROD_ENVIRONMENT = 'production';

const {
    NODE_ENV = 'development',
    SERVER_PORT = 8000,
    SESSION_TTL = 1000*60*2
} = process.env;

exports.IS_IN_PRODUCTION = NODE_ENV === PROD_ENVIRONMENT;

exports.SERVER_OPTIONS = {
    port: SERVER_PORT,
    sessionTTL: SESSION_TTL
  };