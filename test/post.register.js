process.env.NODE_ENV = 'test';

const LoremIpsum = require("lorem-ipsum").LoremIpsum;
const lorem = new LoremIpsum();
const models = require('../models/index');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../src/server');
let should = chai.should();


chai.use(chaiHttp);

describe('User', () => {
    beforeEach((done) => { //Before each test we empty the database
        models.User.destroy(
            {
                where: {},
                truncate: true
            }
        )
        .then(() => {
           done();           
        });        
    });
    describe('/POST auth/register, all field valid', () => {
        it('it should return 200 and an User object', (done) => {
            let user = {
                username: lorem.generateWords(1),
                password: lorem.generateWords(1),
                email: "rick@morty.com"
             
            }
            chai.request(server)
                .post('/auth/register')
                .send(user)
                .end((err, res) => {
                    if (err)
                    console.error(err);
                    res.should.have.status(200);
                done();
                });
        });
    });
    describe('/POST auth/register, missing username', () => {
        it('it should return 400', (done) => {
            let user = {
                password: lorem.generateWords(1),
                email: "rick@morty.com"
             
            }
            chai.request(server)
                .post('/auth/register')
                .send(user)
                .end((err, res) => {
                    if (err)
                    console.error(err);
                    res.should.have.status(400);
                done();
                });
        });
    });
    describe('/POST auth/register, missing password', () => {
        it('it should return 400', (done) => {
            let user = {
                username: lorem.generateWords(1),
                email: "rick@morty.com"
             
            }
            chai.request(server)
                .post('/auth/register')
                .send(user)
                .end((err, res) => {
                    if (err)
                    console.error(err);
                    res.should.have.status(400);
                done();
                });
        });
    });
    describe('/POST auth/register, username already registered', () => {
        it('it should return 400', (done) => {
            let user1 = {
                username: "rick",
                password: lorem.generateWords(1),
                email: "rick@morty.com"
            }
            let user2 = {
                username: "rick",
                password: lorem.generateWords(1),
                email: "rick@morty.com"
            }
            chai.request(server)
                .post('/auth/register')
                .send(user1)
                .end((err, res) => {
                    chai.request(server)
                    .post('/auth/register')
                    .send(user2)
                    .end((err, res) => {
                        if (err)
                        console.error(err);
                        res.should.have.status(422);
                    done();
                    });
                });
        });
    });
});