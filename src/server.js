const express = require('express');
const session = require('express-session');
const redis = require('redis');

const redisClient = redis.createClient();
const RedisStore = require('connect-redis')(session);

const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');

const app = express();
app.set('trust proxy', 1);

const { SERVER_OPTIONS } = require('../config/server');

redisClient.on('error', (err) => {
  // eslint-disable-next-line no-console
  console.error('Redis error: ', err);
});

app.use(session({
  secret: 'ThisOnesNotDirectedByRonHoward',
  name: '_rmRedis',
  resave: false,
  rolling: false,
  saveUninitialized: false,
  cookie: {
    secure: false,
    maxAge: SERVER_OPTIONS.sessionTTL,
  },
  store: new RedisStore({ host: 'localhost', port: 6379, client: redisClient }),
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({ credentials: true }));

app.set('port', SERVER_OPTIONS.port);

const server = http.createServer(app);

require('./controllers/routes')(app);

server.listen(SERVER_OPTIONS.port, () => {
  // eslint-disable-next-line no-console
  console.log('--> Server successfully started at port %d', SERVER_OPTIONS.port);
});

module.exports = app;
