const userController = require('./user_controller');

exports.login = (req, res) => {
  if (req.body && req.body.username && req.body.password) {
    return userController.findByCredentials(req, res);
  } res.send(400);
  return null;
};
exports.logout = (req, res) => {
  req.session.destroy();
  res.end();
  return null;
};

exports.register = (req, res) => {
  if (req.body && req.body.username && req.body.password) {
    return userController.create(req, res);
  }
  res.send(400);
  return null;
};
exports.heartbeat = (req, res) => {
  const isSessionAlive = (req.session && req.session.key);
  if (isSessionAlive) {
    res.status(200).send({
      expires: req.session.cookie.expires,
    });
  } else res.sendStatus(403);
  return isSessionAlive;
};
