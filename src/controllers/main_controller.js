const axios = require('axios');

exports.main = (req, res) => {
  res.status(200).json({
    data: 'Welcome to TechRM API',
  });
};
exports.catchAll = (req, res) => {
  res.status(404).json({
    data: 'Invalid route',
  });
};
exports.protected = (req, res) => {
  if (!req.session || !req.session.key) {
    /*
    res.status(403).send({
      message: 'NOT AUTHENTICATED OR SESSION EXPIRED',
    });
    return;
    */

    // eslint-disable-next-line no-console
    console.error('NOT AUTHENTICATED OR SESSION EXPIRED');
  }
  const hostUrl = 'https://rickandmortyapi.com/api/character/';
  axios.get(hostUrl)
    .then((response) => {
      const outputData = [];
      response.data.results.forEach((dataElement) => {
        const outputElement = {
          name: dataElement.name,
          status: dataElement.status,
          species: dataElement.species,
          gender: dataElement.gender,
          image: dataElement.image,
        };
        outputData.push(outputElement);
      });
      res.status(200).json({
        data: outputData,
      });
    })
    .catch((error) => {
      // eslint-disable-next-line no-console
      console.error(error);
    });
};
