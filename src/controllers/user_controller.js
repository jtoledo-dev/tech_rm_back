const bCrypt = require('bcrypt-nodejs');

const models = require('../../models/index');

exports.generateHash = (password, salt) => bCrypt.hashSync(password, salt, null);
exports.getSalt = () => bCrypt.genSaltSync(8);

exports.findByCredentials = (req, res) => {
  models.User.findOne({
    attributes: ['username', 'salt', 'password'],
    where: { username: req.body.username },
  })
    .then((user) => {
      if (this.generateHash(req.body.password, user.salt) === user.password) {
        req.session.key = user.username;
        res.status(200).send({
          expires: req.session.cookie.expires,
        });
      } else {
        res.status(403).send({
          message: 'INVALID CREDENTIALS',
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'INVALID ID',
      });
    });
};
exports.create = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: 'EMPTY CONTENT',
    });
  }
  models.User.findOne({
    attributes: ['username'],
    where: { username: req.body.username },
  })
    .then((user) => {
      if (user) {
        res.status(422).send({
          message: 'Some error occurred while creating the User.',
        });
      } else {
        const randomSalt = this.getSalt();
        const newUser = new models.User(
          {
            username: req.body.username,
            password: this.generateHash(req.body.password, randomSalt),
            salt: randomSalt,
            email: req.body.email,
          },
        );
        newUser.save()
          .then((userData) => {
            res.send(userData);
          }).catch((err) => {
            res.status(500).send({
              message: err.message || 'Some error occurred while creating the User.',
            });
          });
        return user;
      }
      return null;
    });
  return null;
};
