const main = require('./main_controller');
const authController = require('./authentication_controller');
const userController = require('./user_controller');

module.exports = (app) => {
  app.get('/', main.main);

  app.get('/protected', main.protected);
  app.post('/auth/login', authController.login);
  app.post('/auth/logout', authController.logout);
  app.post('/auth/register', authController.register);
  app.post('/auth/heartbeat', authController.heartbeat);

  app.put('/user', userController.create);
  app.post('/user', userController.findByCredentials);
  app.get('*', main.catchAll);
};
